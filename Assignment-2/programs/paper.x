struct add_in {
	string author<>;
	string title<>;
	string file<>;
};

struct node {
	struct node *next;
	int id;
	string author<>;
	string paper<>;
	string title<>;

};

struct info_out {
	string author<>;
	string title<>;
};

typedef int add_out;
typedef int remove_in;
typedef int info_in;
typedef int fetch_in;
typedef struct node* list_out;
typedef char *fetch_out;


program FUNCTIONS {
    version FUNCTIONS_VERS {
		/* Add function, add papers to the database server */
		add_out ADD_PROC(add_in) = 1;

		/* Remove function, removes paper from the database server */
        int PRINTMESSAGE(info_in) = 2;	

		/* List function, Displays the papers currently stored in the server */
		list_out LIST_PROC()  = 3;

		/* Info function, Display the author and title of a specific paper */
		info_out INFO_PROC(info_in) = 4;

		/* Fetch function, Fetches the content of the paper from server */
		fetch_out FETCH_PROC(fetch_in) = 5;
    } = 1; 								/* Version number */
} = 0x20000001; 						/* Program number */
