/* ############################
Author : Seshagiri Prabhu
RollNo : P2CSN12028
Cyber Security and Networks
seshagiriprabhu@gmail.com
fileName: server.c
##############################*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>

/* function prototype */
void dostuff(int); 

/* Serv_addr will contain the address of the server, and cli_addr will contain the address of the client which connects to the server */
struct sockaddr_in serverAddress, clientAddress;
/* This function is called when a system call fails. It displays a message about the error on stderr and then aborts the program */
void error(char *msg) {
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[]) {

	/* These two variables store the values returned by the socket system call and the accept system call */
	int sockFD, newSockFD;

	/* Stores the port number on which server accepts the connection */
	int portNumber;
	int clientLength, pid;
	
	/* A sockaddr_in is a structure containing an internet address. This structure is defined in <netinet/in.h>. Here's the defenition: 

	struct sockaddr_in {
	 	short   sin_family;
	    u_short sin_port;
	    struct  in_addr sin_addr;
	    char    sin_zero[8];
	}; */
	
	/* The user needs to pass in the port number on which the server will accept connections as an argument. This code displays an error message if the user fails to do this */
    if (argc < 2) {
    	fprintf(stderr,"ERROR, no port provided\n");
        exit(1);
    }

	/* The socket() system call creates a new socket. It takes three arguments. The first is the address domain of the socket (AF_INET = internet), The second argument is the type of socket (Datagram/Stream), Third argument chooses appropriate protocol (TCP/UDP) */
    sockFD = socket(AF_INET, SOCK_STREAM, 0);

    if (sockFD < 0) 
       error("ERROR opening socket");

    /* The function bzero() sets all values in a buffer to zero. It takes two arguments, the first is a pointer to the buffer and the second is the size of the buffer */ 
	bzero((char *) &serverAddress, sizeof(serverAddress));
    portNumber = atoi(argv[1]);
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port = htons(portNumber);

	/* The bind() system call binds a socket to an address */
    if (bind(sockFD, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) < 0) 
    	error("ERROR on binding");

	/* The listen system call allows the process to listen on the socket for connections */
    listen(sockFD,5);
    clientLength = sizeof(clientAddress);

    while (1) {
		/* The accept() system call causes the process to block until a client connects to the server. Thus, it wakes up the process when a connection from a client has been successfully established */
        newSockFD = accept(sockFD, (struct sockaddr *) &clientAddress, &clientLength);

        if (newSockFD < 0) 
        	error("ERROR on accept");
	
		/* Each connection creates a new process */
        pid = fork();

        if (pid < 0)
            error("ERROR on fork");

        if (pid == 0)  {
            close(sockFD);
            dostuff(newSockFD);
            exit(0);
        }

        else 
		 	close(newSockFD);
    } /* end of while */
    return 0; /* we never get here */
}

void dostuff (int sock) {
	/* Return value for read() and write(). i.e it contains no of characters read or written*/
   	int n;

	/* The server reads character fromt the socket connection into this buffer */
   	char buffer[256];
  
   	bzero(buffer,256);
   	n = read(sock,buffer,255);

   	if (n < 0) 
		error("ERROR reading from socket");

   	printf("Here is the message from %d: %s\n", inet_ntoa(clientAddress.sin_addr.s_addr), buffer);
	/* writes a message into the socket */
   	n = write(sock,"I got your message",18);

   	if (n < 0) 
		error("ERROR writing to socket");
}
