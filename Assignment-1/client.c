/* ###########################
Author : Seshagiri Prabhu
Rollno : P2CSN12028
Cyber Security and Networks
seshagiriprabhu@gmail.com
fileName: client.c
##############################*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>

void error(char *msg) {
	perror(msg);
	exit(0);
}

int main(int argc, char *argv[]) {
    int sockFD, portNumber, n;

    struct sockaddr_in serverAddress;
    struct hostent *server;
    char buffer[256];

    if (argc < 3) {
       fprintf(stderr,"usage %s HOSTNAME PORT_NUMBER\n", argv[0]);
       exit(0);
    }

    portNumber = atoi(argv[2]);
    sockFD = socket(AF_INET, SOCK_STREAM, 0);

    if (sockFD < 0) 
        error("ERROR opening socket");

    server = gethostbyname(argv[1]);

    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }

    bzero((char *) &serverAddress, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serverAddress.sin_addr.s_addr,
         server->h_length);
    serverAddress.sin_port = htons(portNumber);

    if (connect(sockFD,(struct sockaddr *)&serverAddress,sizeof(serverAddress)) < 0) 
        error("ERROR connecting");

    printf("Please enter the message: ");
    bzero(buffer,256);
    fgets(buffer,255,stdin);
    n = write(sockFD,buffer,strlen(buffer));

    if (n < 0) 
         error("ERROR writing to socket");

    bzero(buffer,256);
    n = read(sockFD,buffer,255);

    if (n < 0) 
         error("ERROR reading from socket");

    printf("%s\n",buffer);
    return 0;
}
